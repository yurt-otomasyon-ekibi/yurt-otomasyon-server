package com.repository;

import org.springframework.data.repository.CrudRepository;

import com.domain.Ogrenci;

public interface OgrenciRepository extends CrudRepository<Ogrenci, Long> {
	Ogrenci findOgrenciByogrenciID(Long ogrenciID);
	
	Iterable<Ogrenci> findAll();
}
