package com.repository;

import org.springframework.data.repository.CrudRepository;

import com.domain.Bina;

public interface BinaRepository extends CrudRepository<Bina, Long> {
	Iterable<Bina> findAll();
}
