package com.repository;

import org.springframework.data.repository.CrudRepository;

import com.domain.Fatura;

public interface FaturaRepository extends CrudRepository<Fatura, Long> {

}
