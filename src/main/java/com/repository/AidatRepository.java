package com.repository;

import org.springframework.data.repository.CrudRepository;

import com.domain.Aidat;

public interface AidatRepository extends CrudRepository<Aidat, Long> {

}
