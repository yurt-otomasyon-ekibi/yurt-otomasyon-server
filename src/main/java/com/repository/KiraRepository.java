package com.repository;

import org.springframework.data.repository.CrudRepository;

import com.domain.Kira;

public interface KiraRepository extends CrudRepository<Kira, Long> {

}
