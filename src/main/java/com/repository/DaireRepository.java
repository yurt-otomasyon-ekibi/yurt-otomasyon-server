package com.repository;

import org.springframework.data.repository.CrudRepository;

import com.domain.Daire;

public interface DaireRepository extends CrudRepository<Daire, Long> {

}
