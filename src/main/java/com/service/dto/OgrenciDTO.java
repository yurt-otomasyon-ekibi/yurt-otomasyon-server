package com.service.dto;


public class OgrenciDTO {
	
	private Long ogrenciID;
	private String adi;
	private String soyadi;
	private String bolum;
	private DaireDTO daire;
	private String tckimlik;
	private String adres;
	
	public Long getOgrenciID() {
		return ogrenciID;
	}
	public void setOgrenciID(Long ogrenciID) {
		this.ogrenciID = ogrenciID;
	}
	public String getAdi() {
		return adi;
	}
	public void setAdi(String adi) {
		this.adi = adi;
	}
	public String getSoyadi() {
		return soyadi;
	}
	public void setSoyadi(String soyadi) {
		this.soyadi = soyadi;
	}
	public String getBolum() {
		return bolum;
	}
	public void setBolum(String bolum) {
		this.bolum = bolum;
	}
	public DaireDTO getDaire() {
		return daire;
	}
	public void setDaire(DaireDTO daire) {
		this.daire = daire;
	}
	public String getTckimlik() {
		return tckimlik;
	}
	public void setTckimlik(String tckimlik) {
		this.tckimlik = tckimlik;
	}
	public String getAdres() {
		return adres;
	}
	public void setAdres(String adres) {
		this.adres = adres;
	}
	
	
	
}
