package com.service.dto;

import java.sql.Date;

public class AidatDTO {
	private Long aidatID;
	
	private DaireDTO daire;
	
	private Date tarih;
	
	private boolean durum = false;

	public Long getId() {
		return aidatID;
	}

	public void setId(Long id) {
		this.aidatID = id;
	}

	public DaireDTO getDaire() {
		return daire;
	}

	public void setDaire(DaireDTO daire) {
		this.daire = daire;
	}

	public Date getTarih() {
		return tarih;
	}

	public void setTarih(Date tarih) {
		this.tarih = tarih;
	}

	public boolean isDurum() {
		return durum;
	}

	public void setDurum(boolean durum) {
		this.durum = durum;
	}
	
}
