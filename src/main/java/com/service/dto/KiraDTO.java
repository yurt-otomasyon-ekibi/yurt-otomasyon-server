package com.service.dto;

import java.sql.Date;

public class KiraDTO {
	
	private Long kiraID;
	
	private DaireDTO daire;
	
	private OgrenciDTO ogrenci;
	
	private Date tarih;
	
	private boolean durum = false;

	public Long getId() {
		return kiraID;
	}

	public void setId(Long id) {
		this.kiraID = id;
	}

	public DaireDTO getDaire() {
		return daire;
	}

	public void setDaire(DaireDTO daire) {
		this.daire = daire;
	}

	public OgrenciDTO getOgrenci() {
		return ogrenci;
	}

	public void setOgrenci(OgrenciDTO ogrenci) {
		this.ogrenci = ogrenci;
	}

	public Date getTarih() {
		return tarih;
	}

	public void setTarih(Date tarih) {
		this.tarih = tarih;
	}

	public boolean isDurum() {
		return durum;
	}

	public void setDurum(boolean durum) {
		this.durum = durum;
	}
	
	
}
