package com.service.dto;

public class DaireDTO {
	private Long daireID;
	
	private int daire_no;
	
	private BinaDTO bina;
	
	private String elektrik_abone_no;
	
	private String su_abone_no;
	
	private String gaz_abone_no;

	public Long getId() {
		return daireID;
	}

	public void setId(Long id) {
		this.daireID = id;
	}

	public int getDaire_no() {
		return daire_no;
	}

	public void setDaire_no(int daire_no) {
		this.daire_no = daire_no;
	}

	public BinaDTO getBina() {
		return bina;
	}

	public void setBina(BinaDTO bina) {
		this.bina = bina;
	}

	public String getElektrik_abone_no() {
		return elektrik_abone_no;
	}

	public void setElektrik_abone_no(String elektrik_abone_no) {
		this.elektrik_abone_no = elektrik_abone_no;
	}

	public String getSu_abone_no() {
		return su_abone_no;
	}

	public void setSu_abone_no(String su_abone_no) {
		this.su_abone_no = su_abone_no;
	}

	public String getGaz_abone_no() {
		return gaz_abone_no;
	}

	public void setGaz_abone_no(String gaz_abone_no) {
		this.gaz_abone_no = gaz_abone_no;
	}
	
	
}
