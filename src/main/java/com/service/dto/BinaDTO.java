package com.service.dto;

public class BinaDTO {
	
	private Long binaID;
	
	private String bina_adi;

	public Long getId() {
		return binaID;
	}

	public void setId(Long id) {
		this.binaID = id;
	}

	public String getBina_adi() {
		return bina_adi;
	}

	public void setBina_adi(String bina_adi) {
		this.bina_adi = bina_adi;
	}
	
	
}
