package com.service.dto;

import java.sql.Date;

public class FaturaDTO {
	
	private Long faturaID;
	
	private char tip;
	
	private int tutar;
	
	private DaireDTO daire;
	
	private Date tarih;
	
	private boolean durum = false;

	public Long getId() {
		return faturaID;
	}

	public void setId(Long id) {
		this.faturaID = id;
	}

	public char getTip() {
		return tip;
	}

	public void setTip(char tip) {
		this.tip = tip;
	}

	public int getTutar() {
		return tutar;
	}

	public void setTutar(int tutar) {
		this.tutar = tutar;
	}

	public DaireDTO getDaire() {
		return daire;
	}

	public void setDaire(DaireDTO daire) {
		this.daire = daire;
	}

	public Date getTarih() {
		return tarih;
	}

	public void setTarih(Date tarih) {
		this.tarih = tarih;
	}

	public boolean isDurum() {
		return durum;
	}

	public void setDurum(boolean durum) {
		this.durum = durum;
	}
	
	
	
	
}
