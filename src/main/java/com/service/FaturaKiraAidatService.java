package com.service;

import java.util.List;

import com.service.dto.AidatDTO;
import com.service.dto.BinaDTO;
import com.service.dto.DaireDTO;
import com.service.dto.FaturaDTO;
import com.service.dto.KiraDTO;
import com.service.dto.OgrenciDTO;

public interface FaturaKiraAidatService {
	
	//Veri Ekleme
	public void addFatura(FaturaDTO yeni);
	
	public void addKira(KiraDTO kira);
	
	public void addAidat(AidatDTO aidat);
	
	//Fatura Getirme
	public List<FaturaDTO> getFaturasByDaire(DaireDTO daire);
	
	public List<FaturaDTO> getFaturasByTip(String tip);
	
	public List<FaturaDTO> getAllFaturas();
	
	//Aidat Getirme
	public List<AidatDTO> getAidatsByDaire(DaireDTO daire);
	
	public List<AidatDTO> getAidatsByBina(BinaDTO bina);
	
	public List<AidatDTO> getAllAidats();
	
	//Kira Getirme
	public List<KiraDTO> getKirasByOgrenci(OgrenciDTO ogrenci);
	
	public List<KiraDTO> getKirasByDaire(DaireDTO daire);
	
	public List<KiraDTO> getAllKiras();
	
}
