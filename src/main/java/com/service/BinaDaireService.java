package com.service;

import java.util.List;

import com.service.dto.BinaDTO;
import com.service.dto.DaireDTO;

public interface BinaDaireService {
	
	public void addBina(BinaDTO yeni);
	
	public void addDaire(DaireDTO yeni);
	
	public List<DaireDTO> getDairesByBina(BinaDTO bina);
	
	public List<DaireDTO> getAllDaires();
	
	public List<BinaDTO> getAllBinas();

}
