package com.service;

import com.domain.Role;
import com.domain.User;

public interface UserService {
	
	public void authUser(String username, String password);
	
	public void registerUser(User user);
	
	public void addRole(Role role);

}
