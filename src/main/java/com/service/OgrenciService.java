package com.service;

import java.util.List;

import com.service.dto.OgrenciDTO;

public interface OgrenciService {
	
	public void ogrenciEkle(OgrenciDTO ogrenci);
	
	public void ogrenciDuzenle(OgrenciDTO ogrenci);
	
	public OgrenciDTO ogrenciGetir();
	
	public List<OgrenciDTO> ogrencileriGetir();
	
}
