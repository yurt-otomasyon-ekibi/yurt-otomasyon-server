package com.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domain.Ogrenci;
import com.repository.OgrenciRepository;
import com.service.OgrenciService;
import com.service.dto.OgrenciDTO;

@Service
public class OgrenciServiceImpl implements OgrenciService {
	
	ModelMapper mapper = new ModelMapper();
	
	@Autowired
	OgrenciRepository ogrenciRepository;

	public void ogrenciEkle(OgrenciDTO ogrenci) {
		ogrenciRepository.save(mapper.map(ogrenci, Ogrenci.class));
	}

	public OgrenciDTO ogrenciGetir() {
		return null;
	}

	public List<OgrenciDTO> ogrencileriGetir() {
		Iterable<Ogrenci> ogrenciList = this.ogrenciRepository.findAll();
		List<OgrenciDTO> ogrencilerDTO = new ArrayList<OgrenciDTO>();
		for(Ogrenci entity : ogrenciList){
			OgrenciDTO dto = mapper.map(entity, OgrenciDTO.class);
			ogrencilerDTO.add(dto);
		}
		return ogrencilerDTO;
	}

	public void ogrenciDuzenle(OgrenciDTO ogrenci) {
		ogrenciRepository.save(mapper.map(ogrenci, Ogrenci.class));
		
	}

}
