package com.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domain.Role;
import com.domain.User;
import com.repository.RoleRepository;
import com.repository.UserRepository;
import com.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	RoleRepository roleRepository;

	public void authUser(String username, String password) {
		// TODO Auto-generated method stub
		
	}

	public void registerUser(User user) {
		userRepository.save(user);
	}

	public void addRole(Role role) {
		roleRepository.save(role);
	}

}
