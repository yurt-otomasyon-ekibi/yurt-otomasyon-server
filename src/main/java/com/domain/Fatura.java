package com.domain;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_fatura", schema = "public")
public class Fatura {
	
	@Column(nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long faturaID;
	
	@Column(name = "fatura_tip", nullable = false)
	private char tip;
	
	@Column(name = "tutar", nullable = false)
	private int tutar;
	
	@JoinColumn(name = "daireID", nullable = false)
	@ManyToOne(optional = false, cascade = CascadeType.ALL)
	private Daire daire;
	
	@Column(name = "tarih", nullable = false)
	private Date tarih;
	
	@Column(name = "durum")
	private boolean durum = false;

	public Long getId() {
		return faturaID;
	}

	public void setId(Long id) {
		this.faturaID = id;
	}

	public char getTip() {
		return tip;
	}

	public void setTip(char tip) {
		this.tip = tip;
	}

	public int getTutar() {
		return tutar;
	}

	public void setTutar(int tutar) {
		this.tutar = tutar;
	}

	public Daire getDaire() {
		return daire;
	}

	public void setDaire(Daire daire) {
		this.daire = daire;
	}

	public Date getTarih() {
		return tarih;
	}

	public void setTarih(Date tarih) {
		this.tarih = tarih;
	}
	
	
	
}
