package com.domain;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_aidat", schema = "public")
public class Aidat {
	
	@Column(nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long aidatID;
	
	@JoinColumn(name = "daireID", nullable = false)
	@ManyToOne(optional = false, cascade = CascadeType.ALL)
	private Daire daire;
	
	@Column(name = "tarih", nullable = false)
	private Date tarih;
	
	@Column(name = "durum")
	private boolean durum = false;

	public Long getId() {
		return aidatID;
	}

	public void setId(Long id) {
		this.aidatID = id;
	}

	public Daire getDaire() {
		return daire;
	}

	public void setDaire(Daire daire) {
		this.daire = daire;
	}

	public Date getTarih() {
		return tarih;
	}

	public void setTarih(Date tarih) {
		this.tarih = tarih;
	}

	public boolean isDurum() {
		return durum;
	}

	public void setDurum(boolean durum) {
		this.durum = durum;
	}
	
}
