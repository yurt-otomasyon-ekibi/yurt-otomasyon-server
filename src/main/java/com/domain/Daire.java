package com.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "tbl_daire", schema = "public")
public class Daire {
	
	@Column(nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long daireID;
	
	@Column(name = "daire_no", nullable = false)
	private int daire_no;
	
	@JoinColumn(name = "binaID", nullable = false)
	@ManyToOne(optional = false, cascade = CascadeType.ALL)
	private Bina bina;
	
	@Column(name = "elektrik_abone_no", nullable = false)
	private String elektrik_abone_no;
	
	@Column(name = "su_abone_no", nullable = false)
	private String su_abone_no;
	
	@Column(name = "gaz_abone_no", nullable = false)
	private String gaz_abone_no;

	public Long getId() {
		return daireID;
	}

	public void setId(Long id) {
		this.daireID = id;
	}

	public int getDaire_no() {
		return daire_no;
	}

	public void setDaire_no(int daire_no) {
		this.daire_no = daire_no;
	}

	public Bina getBina() {
		return bina;
	}

	public void setBina(Bina bina) {
		this.bina = bina;
	}

	public String getElektrik_abone_no() {
		return elektrik_abone_no;
	}

	public void setElektrik_abone_no(String elektrik_abone_no) {
		this.elektrik_abone_no = elektrik_abone_no;
	}

	public String getSu_abone_no() {
		return su_abone_no;
	}

	public void setSu_abone_no(String su_abone_no) {
		this.su_abone_no = su_abone_no;
	}

	public String getGaz_abone_no() {
		return gaz_abone_no;
	}

	public void setGaz_abone_no(String gaz_abone_no) {
		this.gaz_abone_no = gaz_abone_no;
	}
	
	
}
