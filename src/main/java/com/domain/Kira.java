package com.domain;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "tbl_kira", schema = "public")
public class Kira {
	
	@Column(nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long kiraID;
	
	@JoinColumn(name = "ogrenciID", nullable = true)
	@ManyToOne(optional = false, cascade = CascadeType.ALL)
	private Ogrenci ogrenci;
	
	@Column(name = "tarih", nullable = false)
	private Date tarih;
	
	@Column(name = "durum")
	private boolean durum = false;

	public Long getId() {
		return kiraID;
	}

	public void setId(Long id) {
		this.kiraID = id;
	}

	public Ogrenci getOgrenci() {
		return ogrenci;
	}

	public void setOgrenci(Ogrenci ogrenci) {
		this.ogrenci = ogrenci;
	}

	public Date getTarih() {
		return tarih;
	}

	public void setTarih(Date tarih) {
		this.tarih = tarih;
	}

	public boolean isDurum() {
		return durum;
	}

	public void setDurum(boolean durum) {
		this.durum = durum;
	}
}
