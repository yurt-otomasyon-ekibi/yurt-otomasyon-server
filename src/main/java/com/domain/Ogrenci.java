package com.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_ogrenci", schema = "public")
public class Ogrenci {
	
	@Column(nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ogrenciID;
	
	@Column(name = "adi", nullable = false)
	private String adi;
	
	@Column(name = "soyadi", nullable = false)
	private String soyadi;
	
	@Column(name = "bolum", nullable = false)
	private String bolum;
	
	@JoinColumn(name = "daireID", nullable = false)
	@ManyToOne(optional = false, cascade = CascadeType.ALL)
	private Daire daire;
	
	@Column(name = "tckimlik", nullable = false)
	private String tckimlik;
	
	@Column(name = "adres", nullable = false)
	private String adres;

	public Long getId() {
		return ogrenciID;
	}

	public void setId(Long id) {
		this.ogrenciID = id;
	}

	public String getAdi() {
		return adi;
	}

	public void setAdi(String adi) {
		this.adi = adi;
	}

	public String getSoyadi() {
		return soyadi;
	}

	public void setSoyadi(String soyadi) {
		this.soyadi = soyadi;
	}

	public String getBolum() {
		return bolum;
	}

	public void setBolum(String bolum) {
		this.bolum = bolum;
	}

	public Daire getDaire() {
		return daire;
	}

	public void setDaire(Daire daire) {
		this.daire = daire;
	}

	public String getTckimlik() {
		return tckimlik;
	}

	public void setTckimlik(String tckimlik) {
		this.tckimlik = tckimlik;
	}

	public String getAdres() {
		return adres;
	}

	public void setAdres(String adres) {
		this.adres = adres;
	}
	
	
	
	
}
