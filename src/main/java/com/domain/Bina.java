package com.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_bina", schema = "public")
public class Bina {
	
	@Column(nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long binaID;
	
	@Column(name = "bina_adi", nullable = false)
	private String bina_adi;

	public Long getId() {
		return binaID;
	}

	public void setId(Long id) {
		this.binaID = id;
	}

	public String getBina_adi() {
		return bina_adi;
	}

	public void setBina_adi(String bina_adi) {
		this.bina_adi = bina_adi;
	}
	
	

}
