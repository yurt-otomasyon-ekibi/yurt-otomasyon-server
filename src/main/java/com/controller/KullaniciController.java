package com.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.domain.Ogrenci;
import com.service.OgrenciService;
import com.service.dto.OgrenciDTO;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping(value = "/user")
public class KullaniciController {
	
	List<OgrenciDTO> list = new ArrayList<OgrenciDTO>();
	
	@Autowired
	OgrenciService ogrenciService;
	
	@RequestMapping(value = "/getUser", method = RequestMethod.GET)
	public ResponseEntity<Ogrenci> getUser(){
		Ogrenci ogrenci = new Ogrenci();
		return ResponseEntity.status(200).body(ogrenci);
	}
	
	@RequestMapping(value = "/ogrenciEkle", method = RequestMethod.POST)
	public ResponseEntity<OgrenciDTO> addUser(@RequestBody OgrenciDTO ogrenci){
		ogrenciService.ogrenciEkle(ogrenci);
		return ResponseEntity.status(201).body(ogrenci);
	}
	
	@RequestMapping(value = "/ogrenciDuzenle", method = RequestMethod.POST)
	public ResponseEntity<?> editUser(@RequestBody OgrenciDTO ogrenci){
		ogrenciService.ogrenciDuzenle(ogrenci);
		return ResponseEntity.status(200).body(ogrenci);
	}
	
	@RequestMapping(value = "/ogrenciler", method = RequestMethod.GET)
	public ResponseEntity<List<OgrenciDTO>> getAllOgrenci(){
		List<OgrenciDTO> list = ogrenciService.ogrencileriGetir();
		return ResponseEntity.status(200).body(list);
	}
}
